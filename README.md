# Redis Message Forwarder

Rdfwd forwards messages from one or more [Redis](https://redis.io) Pub/Sub channels to a TCP server.

## Usage

``` shellsession
$ ./rdfwd --help
Usage: rdfwd [options] host:port chan [chan ...]

Forward all messages from the listed Redis Pub/Sub channels to the TCP
server listening at host:port.
  -msgTerm string
    	Message termination string (default "\r\n")
  -rdaddr string
    	Redis server host:port (default "localhost:6379")
  -version
    	Show program version information and exit
```
