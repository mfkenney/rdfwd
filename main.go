// Rdfwd forwards the messsages from one or more Redis Pub/Sub channels to
// a TCP server.
package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"

	"github.com/gomodule/redigo/redis"
)

const Usage = `Usage: rdfwd [options] host:port chan [chan ...]

Forward all messages from the listed Redis Pub/Sub channels to the TCP
server listening at host:port.
`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	rdAddr  string = "localhost:6379"
	msgTerm string = "\r\n"
	msgQlen int    = 10
)

func lookupEnvOrString(key string, defaultVal string) string {
	if val, ok := os.LookupEnv(key); ok {
		return val
	}
	return defaultVal
}

func lookupEnvOrInt(key string, defaultVal int) int {
	if val, ok := os.LookupEnv(key); ok {
		v, err := strconv.Atoi(val)
		if err != nil {
			log.Fatalf("LookupEnvOrInt[%s]: %v", key, err)
		}
		return v
	}
	return defaultVal
}

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&rdAddr, "rdaddr",
		lookupEnvOrString("REDIS_SERVER", rdAddr),
		"Redis server host:port")
	flag.StringVar(&rdAddr, "msgTerm",
		lookupEnvOrString("MSG_TERM", msgTerm),
		"Message termination string")
	flag.IntVar(&msgQlen, "qlen",
		lookupEnvOrInt("RDFWD_QLEN", msgQlen),
		"message queue length")

	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

// Pass all received Redis pub-sub messages to a Go channel.
func pubsubReader(psc redis.PubSubConn, qlen int) <-chan redis.Message {
	c := make(chan redis.Message, qlen)
	go func() {
		defer close(c)
		for {
			switch msg := psc.Receive().(type) {
			case error:
				log.Println(msg)
				return
			case redis.Subscription:
				if msg.Count == 0 {
					log.Println("Pubsub channel closed")
					return
				} else {
					log.Printf("Subscribed to %q", msg.Channel)
				}
			case redis.Message:
				select {
				case c <- msg:
				default:
					log.Printf("Queue full, %q message dropped", msg.Data)
				}
			}
		}
	}()

	return c
}

func main() {
	args := parseCmdLine()

	if len(args) < 2 {
		flag.Usage()
		os.Exit(1)
	}

	conn, err := net.Dial("tcp", args[0])
	if err != nil {
		log.Fatalf("Cannot connect to %s: %v", args[0], err)
	}
	defer conn.Close()

	rdconn, err := redis.Dial("tcp", rdAddr)
	if err != nil {
		log.Fatalf("Cannot connect to Redis: %v", err)
	}
	defer rdconn.Close()

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	psc := redis.PubSubConn{Conn: rdconn}
	ch := pubsubReader(psc, msgQlen)
	for _, arg := range args[1:] {
		psc.Subscribe(arg)
	}

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			psc.Unsubscribe()
		}
	}()

	log.Printf("Redis message forwarder (%s)", Version)

	eom := []byte(msgTerm)
	for msg := range ch {
		conn.Write(msg.Data)
		_, err := conn.Write(eom)
		if err != nil {
			log.Printf("Server write error: %v", err)
			psc.Unsubscribe()
			break
		}
	}
}
